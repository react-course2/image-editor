import React from 'react';

export default function ImageLoader(props) {
  function onImageSourceChanged(event) {
    props.setImageSource(event.target.value);
  }

  return (
    <>
      <h4>ENTER IMAGE URL</h4>
      <input type="text" onChange={onImageSourceChanged} />
    </>
  );
}

