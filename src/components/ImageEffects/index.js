import React from 'react';

export default function ImageEffects({ rotateImage, setBlur }) {
  function onChangeBlurValue(event) {
    setBlur(event.target.value);
  }

  return (
    <>
      <h4>Rotation</h4>
      <hr />
      <button onClick={rotateImage}>ROTATE CLOCKWISE</button>
      <hr />
      <input type="number" onChange={onChangeBlurValue} />
    </>
  );
}
