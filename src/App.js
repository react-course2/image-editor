import React, { Component } from 'react';
import ImageLoader from './components/ImageLoader';
import './App.css';
import ImageEffects from './components/ImageEffects';
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      imageSource: '',
      imageEffects: { rotation: 0, blur: 0 },
    };
    this.updateImageSource = this.updateImageSource.bind(this);
    this.rotateClockwise = this.rotateClockwise.bind(this);
    this.setBlur = this.setBlur.bind(this);
  }

  updateImageSource(img) {
    this.setState({ imageSource: img });
  }

  rotateClockwise() {
    this.setState({
      imageEffects: {
        ...this.state.imageEffects,
        rotation: this.state.imageEffects.rotation + 90,
      },
    });
  }

  setBlur(blur) {
    this.setState({
      imageEffects: { ...this.state.imageEffects, blur },
    });
  }

  render() {
    return (
      <>
        <div
          className="row"
          style={{
            flex: 1,
            backgroundColor: '#efefef',
          }}
        >
          <div className="col" style={{ flex: 1 }}>
            <ImageLoader
              setImageSource={this.updateImageSource}
              text={<h4>IMAGE</h4>}
            />
            <img
              alt=""
              src={this.state.imageSource}
              style={{
                width: 500,
                height: 500,
                objectFit: 'cover',
                transform: `rotate(${this.state.imageEffects.rotation}deg)`,
                filter: `blur(${this.state.imageEffects.blur}px)`,
              }}
            />
          </div>

          <div className="col" style={{ flex: 1 }}>
            <ImageEffects
              rotateImage={this.rotateClockwise}
              setBlur={this.setBlur}
            />
          </div>
        </div>
      </>
    );
  }
}
